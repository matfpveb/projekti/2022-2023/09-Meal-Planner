import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';

import baseController from './controllers';

import handle404 from './middlewares/handle404';
import handleError from './middlewares/handleError';

dotenv.config();

const app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(express.static('public'));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(baseController);
app.use(handle404);
app.use(handleError);

app.listen(process.env.PORT, async () => {
  if (!process.env.MONGODB_CONNECTION) {
    console.error('Missing env variable: MONGODB_CONNECTION');
    process.exit(1);
  }
  await mongoose.connect(process.env.MONGODB_CONNECTION);
  console.log(`Server listening on port ${process.env.PORT}.`);
});
