import { User } from '../models/user';
import bcrypt from 'bcrypt';

import { UserRole } from '../types';

async function run() {
  const adminUser = new User({
    username: 'admin2',
    email: 'admin2@mealplanner.app',
    password: await bcrypt.hash('Admin123.', 12),
    role: UserRole.Admin,
  });

  await adminUser.save();

  return adminUser;
}

export default run;
