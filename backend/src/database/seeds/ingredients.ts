import * as fs from 'fs';
import * as path from 'path';
import * as csv from 'fast-csv';

import { Ingredient } from '../models/ingredient';

type Row = {
  Category: string;
  Description: string;
  'Data.Carbohydrate': string;
  'Data.Protein': string;
  'Data.Fat.Total.Lipid': string;
};
function parseCSV() {
  return new Promise<Row[]>((resolve, reject) => {
    const data: Row[] = [];
    fs.createReadStream(
      path.resolve(__dirname, '../../../data', 'ingredients_modified.csv')
    )
      .pipe(csv.parse({ headers: true }))
      .on('error', (error) => reject(error))
      .on('data', (row: Row) => data.push(row))
      .on('end', () => resolve(data));
  });
}

async function run() {
  const parsedCSV = await parseCSV();
  await Ingredient.create(
    parsedCSV.map((row) => ({
      name: row.Description,
      category: row.Category,
      nutritiveValues: {
        protein: parseFloat(row['Data.Protein']),
        carbs: parseFloat(row['Data.Carbohydrate']),
        fat: parseFloat(row['Data.Fat.Total.Lipid']),
      },
    }))
  );
}

export default run;
