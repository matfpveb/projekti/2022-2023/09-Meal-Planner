import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

import adminUserSeeder from './adminUser';
import ingredientsSeeder from './ingredients';

async function main() {
  await mongoose.connect(process.env.MONGODB_CONNECTION!);
  await adminUserSeeder();
  await ingredientsSeeder();
}

main()
  .then(() => {
    console.log('Successfully seeded database.');
    process.exit(0);
  })
  .catch((e) => {
    console.log('Error while seeding the database');
    console.log(e);
    process.exit(1);
  });
