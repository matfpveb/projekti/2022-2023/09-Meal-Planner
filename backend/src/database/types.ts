export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}
export enum IngredientCategory {
  Butter = 'Butter',
  MilkProduct = 'MilkProduct',
  EggProduct = 'EggProduct',
  Topping = 'Topping',
  Other = 'Other',
  Spice = 'Spice',
  MeetProduct = 'MeetProduct',
}

export enum MeasureUnit {
  G = 'G',
  Kg = 'Kg',
  L = 'L',
  'Ml' = 'Ml',
  Tsp = 'Tsp',
  Tbsp = 'Tbsp',
}
