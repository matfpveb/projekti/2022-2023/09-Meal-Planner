import { Types, Schema, model } from 'mongoose';

import { MeasureUnit } from '../types';
import { User } from './user';

export interface RecipeSchema {
  name: string;
  author: Types.ObjectId;
  img: string | null;
  ingredients: {
    ingredient: Types.ObjectId;
    amount: number;
    unit: MeasureUnit;
  }[];
  steps: string[];
  likes: Types.ObjectId[];
  comments: {
    user: Types.ObjectId;
    content: string;
  }[];
}

const recipeSchema = new Schema<RecipeSchema>({
  name: {
    type: Schema.Types.String,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  img: {
    type: Schema.Types.String,
    default: null,
  },
  ingredients: {
    type: [
      {
        ingredient: {
          type: Schema.Types.ObjectId,
          ref: 'Ingredient',
          required: true,
        },
        amount: {
          type: Schema.Types.Number,
          required: true,
        },
        unit: {
          type: Schema.Types.String,
          required: true,
          enum: MeasureUnit,
        },
      },
    ],
    required: true,
    default: [],
  },
  steps: {
    type: [Schema.Types.String],
    required: true,
  },
  likes: {
    type: [Schema.Types.ObjectId],
    required: true,
    ref: User.modelName,
    default: [],
  },
  comments: {
    type: [
      {
        user: {
          type: Schema.Types.ObjectId,
          ref: User.modelName,
          required: true,
        },
        content: {
          type: Schema.Types.String,
          required: true,
        },
      },
    ],
    required: true,
    default: [],
  },
});

export const Recipe = model('Recipe', recipeSchema);
