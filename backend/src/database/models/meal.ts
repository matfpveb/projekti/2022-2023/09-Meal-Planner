import { Types, Schema, model } from 'mongoose';
import { RecipeSchema } from './recipe';

export interface MealSchema {
  date: string;
  mealType: 'breakfast' | 'snack' | 'lunch' | 'dinner';
  recipe: Types.ObjectId;
}

const mealSchema = new Schema<MealSchema>({
  date: {
    type: String,
    required: true,
  },
  mealType: {
    type: String,
    enum: ['breakfast', 'snack', 'lunch', 'dinner'],
    required: true,
  },
  recipe: {
    type: Schema.Types.ObjectId,
    ref: 'Recipe',
    required: true,
  },
});

export const Meal = model('Meal', mealSchema);
