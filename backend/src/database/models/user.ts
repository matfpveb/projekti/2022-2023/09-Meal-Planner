import { Schema, model, Types } from 'mongoose';

import { UserRole } from '../types';

export interface UserSchema {
  username: string;
  email: string;
  password?: string;
  role: UserRole;
  recipes: Types.ObjectId[];
  likedRecipes: Types.ObjectId[];
}

const userSchema = new Schema<UserSchema>({
  username: {
    type: Schema.Types.String,
    index: { unique: true },
    required: true,
  },
  email: { type: Schema.Types.String, index: { unique: true }, required: true },
  password: { type: Schema.Types.String, required: true, select: false },
  role: {
    type: Schema.Types.String,
    enum: UserRole,
    default: UserRole.User,
    required: true,
  },
  recipes: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Recipe',
      },
    ],
    required: true,
    default: [],
  },
  likedRecipes: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Recipe',
      },
    ],
    required: true,
    default: [],
  },
});

export const User = model<UserSchema>('User', userSchema);
