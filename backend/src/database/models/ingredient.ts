import { model, Schema } from 'mongoose';
import { IngredientCategory } from '../types';

export interface IngredientSchema {
  name: string;
  category: IngredientCategory;
  // Nutritive values in 100 grams of the product expressed in grams.
  nutritiveValues: {
    protein: number;
    carbs: number;
    fat: number;
  };
}

const schema = new Schema<IngredientSchema>({
  name: { type: Schema.Types.String, required: true },
  category: {
    type: Schema.Types.String,
    enum: IngredientCategory,
    required: true,
  },
  nutritiveValues: {
    protein: { type: Schema.Types.Number, required: true },
    carbs: { type: Schema.Types.Number, required: true },
    fat: { type: Schema.Types.Number, required: true },
  },
});

export const Ingredient = model<IngredientSchema>('Ingredient', schema);
