import { NextFunction, Request, Response } from 'express';

function handle404(req: Request, res: Response, next: NextFunction) {
  res.status(404).json({ message: 'Route not found' });
}

export default handle404;
