import { NextFunction, Request, Response } from 'express';
import { ValidationError } from 'yup';
import multer from 'multer';

function handleError(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (err instanceof ValidationError) {
    return res
      .status(400)
      .json({ message: err.message, errors: err.inner.map((e) => e.message) });
  }

  if (err instanceof multer.MulterError) {
    return res
      .status(500)
      .json({ message: err.message, code: err.code, name: err.name });
  }
  console.log(err);
  return res.status(500).json({ message: 'Unknown error occurred' });
}

export default handleError;
