import { NextFunction, Request, Response } from 'express';
import { BaseSchema } from 'yup';
import { AnyObject, ValidateOptions } from 'yup/es/types';

function validate<S extends BaseSchema, C extends AnyObject>(
  schema: S,
  validateOptions?: ValidateOptions<C>
) {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await schema.validate(req, { abortEarly: false, ...validateOptions });
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  };
}

export default validate;
