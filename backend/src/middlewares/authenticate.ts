import { Response, NextFunction, Request } from 'express';

// DB
import { User } from '../database/models/user';

// Util
import { verifyJWT } from '../util/jwt';
import { UserRole } from '../database/types';


/**
 * Ovaj middleware se moze koristiti da se zastiti HTTP ruta za koju korisnik mora
 * biti autentikovan. Kada se ovaj middleware postavi ispred glavnog hendlera HTTP
 * zahteva, autentikovan korisnik se moze dobiti kao `req.user`.
 */
function authenticate(role?: UserRole) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers['authorization'];
    //console.log(req);
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) return res.sendStatus(401);

    try {
      const payload = verifyJWT(token);

      const currentUser = await User.findOne({ username: payload.username });

      if (!currentUser) {
        return res.sendStatus(401);
      }

      if (role && currentUser.role != role) {
        return res.sendStatus(403);
      }

      req.user = currentUser;

      next();
    } catch (e) {
      res.sendStatus(401);
    }
  };
}

export default authenticate;
