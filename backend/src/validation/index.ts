import { number, string, ValidationError } from 'yup';

import { User } from '../database/models/user';

export const isUsername = (checkUnique = true) =>
  string()
    .required()
    .min(5)
    .max(30)
    .test('is-unique-username', async (username) => {
      if (checkUnique) {
        if (await User.findOne({ username })) {
          throw new ValidationError('User already exists');
        }
      }

      return true;
    });
export const isPassword = () => string().required().min(7).max(30);
export const isEmail = (checkUnique = true) =>
  string()
    .email()
    .required()
    .test('is-unique-email', async (email) => {
      if (checkUnique) {
        if (await User.findOne({ email })) {
          throw new ValidationError('Email already exists');
        }
      }
      return true;
    });
export const isNutritiveValue = () => number().positive().max(100);
export const isMongoId = () =>
  string()
    .required()
    .matches(/^(0x|0h)?[0-9A-F]+$/i)
    .length(24);
export const isIngredientName = () => string().min(3).max(30).required();
export const isRecipeName = () => string().min(3).max(30).required();
const validMealTypes = ['breakfast', 'snack', 'lunch', 'dinner'];
export const isMealType = () => string().oneOf(validMealTypes, 'Invalid meal type.').required();