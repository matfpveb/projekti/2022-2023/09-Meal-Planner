import { HydratedDocument } from 'mongoose';

import { UserSchema } from '../../database/models/user';

declare global {
  declare namespace Express {
    export interface Request {
      user: HydratedDocument<UserSchema>;
    }
  }
}
