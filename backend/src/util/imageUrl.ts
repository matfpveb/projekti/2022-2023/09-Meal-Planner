import { Request } from 'express';

function imageUrl(req: Request, filename: string) {
  return `${req.protocol}://${req.get('host')}/images/${filename}`;
}

export default imageUrl;
