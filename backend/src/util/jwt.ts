import jwt from 'jsonwebtoken';

type TokenPayload = jwt.JwtPayload & {
  username: string;
};

export function generateJWT(payload: TokenPayload) {
  return jwt.sign(payload, process.env.JWT_SECRET as string, {
    expiresIn: process.env.JWT_EXPIRES_IN as string,
  });
}

export function verifyJWT(token: string): TokenPayload {
  const payload = jwt.verify(token, process.env.JWT_SECRET as string);

  if (typeof payload === 'string') {
    throw new Error('Token not valid.');
  }

  if (!payload.username) {
    throw new Error('Token not valid.');
  }

  return {
    username: payload.username,
  };
}
