import { Request, Response, Router } from 'express';
import { HydratedDocument } from 'mongoose';
import * as yup from 'yup';
import { nanoid } from 'nanoid';

// DB
import { Meal, MealSchema } from '../database/models/meal';

// Middlewares
import authenticate from '../middlewares/authenticate';
import validate from '../middlewares/validate';

// Validators
import { isMongoId, isMealType } from '../validation';
import { Recipe } from '../database/models/recipe';
import { Ingredient } from '../database/models/ingredient';

const mealController = Router();

type GetAllRequest = Request<
  { id?: string },
  HydratedDocument<MealSchema>[] | HydratedDocument<MealSchema>,
  {}
>;

mealController.get(
  '/:id?',
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId().optional(),
      }),
    })
  ),
  async (req: GetAllRequest, res, next) => {
    try {
      const { id } = req.params;
      const query = id ? { _id: id } : {};
      const data = await Meal.find(query).populate('recipe');
      if (!data) return res.sendStatus(404);
      return res.status(200).json(data);
    } catch (e) {
      next(e);
    }
  }
);

type CreateRequest = Request<{}, HydratedDocument<MealSchema>>;

mealController.post(
  '/',
  authenticate(),
 validate(
    yup.object().shape({
      body: yup.object().shape({
        date: yup.string().matches(/^(19|20)\d{2}-(0[1-9]|1[0-2])-([0-2][1-9]|3[0-1])$/).required(),
        mealType: isMealType().required(),
        recipeId: isMongoId().required(),
      }),
})
 ),
  async (req: CreateRequest, res:Response, next:any) => {
    try {
      const { date, mealType, recipeId } = req.body;
      const recipeI = await Recipe.findById(recipeId);
      if (!recipeI) return res.sendStatus(404);
       const meal = new Meal({ date, mealType, recipe: recipeId });
       await meal.save();

      res.status(201).json(meal);
    } catch (e) {
      next(e);
    }
  }
);

type UpdateRequest = Request<{ id: string }, HydratedDocument<MealSchema>, Partial<MealSchema>>;

mealController.patch(
  '/:id',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
      body: yup.object().shape({
        date: yup.date().optional(),
        mealType: isMealType().optional(),
        recipeId: isMongoId().optional(),
      }),
    })
  ),
  async (req: UpdateRequest, res:any, next:any) => {
    try {
      const { id } = req.params;
      const meal = await Meal.findById(id);
      if (!meal) return res.sendStatus(404);

      if (req.body.recipe) {
        const recipeI = await Recipe.findById(req.body.recipe);
        if (!recipeI) return res.sendStatus(404);
      }

      meal.set(req.body);
      await meal.save();

      res.status(200).json(meal);
    } catch (e) {
      next(e);
    }
  }
);

type DeleteRequest = Request<{ id: string }, HydratedDocument<MealSchema>>;

mealController.delete(
  '/:id',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
    })
  ),
  async (req: DeleteRequest, res, next) => {
    try {
      const { id } = req.params;
      const meal = await Meal.findById(id);
      if (!meal) return res.sendStatus(404);

      await Meal.deleteOne({ _id: meal._id });

      res.status(200).json(meal);
    } catch (e) {
      next(e);
    }
  }
);

mealController.get(
  '/ingridients/:date',
 // authenticate(),

async (req: any, res:any, next:any) => {
  const {date} = req.params;
  console.log(date);
  const ingridients: any = [];
  const meals:any = await Meal.find({date:date}).populate('recipe');
 
  for(let meal of meals){
    for(let ing of meal.recipe.ingredients){
      const ingridient:any = await Ingredient.findById({_id: ing.ingredient});
      ingridients.push({
        name: ingridient.name,
        unit: ing.unit,
        amount: ing.amount
      })
    }
  }
  res.status(200).json(ingridients)
}
);
export default mealController;
