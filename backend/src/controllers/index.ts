import { Router } from 'express';

// Controllers
import userController from './user';
import authController from './auth';
import ingredientsController from './ingredients';
import recipesController from './recipes';
import mealController from './meals';


const baseController = Router();

baseController.get('/welcome', (req, res) => {
  res.send('Welcome to Meal Planner API!');
});

baseController.use('/auth', authController);
baseController.use('/user', userController);
baseController.use('/ingredients', ingredientsController);
baseController.use('/recipes', recipesController);
baseController.use('/meals', mealController);

export default baseController;
