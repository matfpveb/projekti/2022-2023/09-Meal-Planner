import { Request, Response, Router } from 'express';
import { HydratedDocument } from 'mongoose';
import * as yup from 'yup';

// DB
import { User, UserSchema } from '../database/models/user';
import { Recipe } from '../database/models/recipe';

// Middlewares
import authenticate from '../middlewares/authenticate';
import validate from '../middlewares/validate';

// Validation
import { isMongoId } from '../validation';

const userController = Router();
type GetAll = Request<{}, HydratedDocument<UserSchema>[], {}>;
userController.get('/', async (req: GetAll, res, next) => {
  const users = await User.find();
  res.status(200).json(users);
});

type GetCurrentUserRequest = Request<{}, HydratedDocument<UserSchema>>;
userController.get(
  '/current',
  authenticate(),
  async (req: GetCurrentUserRequest, res: Response, next) => {
    try {
      await req.user.populate('recipes');
      await req.user.populate('recipes.ingredients.ingredient');
      res.status(200).json(req.user);
    } catch (e) {
      next(e);
    }
  }
);
userController.get('/user/current', authenticate, (req:Request, res: Response) => {
  const userId = req.user._id;
  
  try{
    res.status(200).json(userId);
  }catch (e) {
    
  }
  
});

type LikeRequest = Request<{ recipeId: string }, true>;
userController.put(
  '/current/like/:recipeId',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        recipeId: isMongoId(),
      }),
    })
  ),
  async (req: LikeRequest, res, next) => {
    try {
      const recipe = await Recipe.findById(req.params.recipeId);
      if (!recipe) return res.sendStatus(404);

      recipe.likes.push(req.user.id);
      await recipe.save();

      req.user.likedRecipes.push(recipe.id);
      await req.user.save();

      return res.status(200).json(true);
    } catch (e) {
      next(e);
    }
  }
);

type UnlikeRequest = LikeRequest;

userController.put(
  '/current/unlike/:recipeId',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        recipeId: isMongoId(),
      }),
    })
  ),
  async (req: UnlikeRequest, res: Response, next) => {
    try {
      const recipe = await Recipe.findById(req.params.recipeId);
      if (!recipe) return res.sendStatus(404);

      recipe.likes = recipe.likes.filter(
        (user) => user.toString() !== req.user.id
      );
      await recipe.save();

      req.user.likedRecipes = req.user.likedRecipes.filter(
        (r) => r.toString() !== recipe.id
      );
      await req.user.save();

      return res.status(200).json(true);
    } catch (e) {
      next(e);
    }
  }
);

type CommentRequest = Request<{ recipeId: string }, true, { content: string }>;

userController.put(
  '/current/comment/:recipeId',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        recipeId: isMongoId(),
      }),
    })
  ),
  async (req: CommentRequest, res, next) => {
    try {
      const recipe = await Recipe.findById(req.params.recipeId);
      if (!recipe) return res.sendStatus(404);

      recipe.comments.push({ user: req.user.id, content: req.body.content });
      await recipe.save();

      res.status(200).json(true);
    } catch (e) {
      next(e);
    }
  }
);

type GetUserRequest = Request<{ userId: string }, HydratedDocument<UserSchema>>;

userController.get(
  '/:userId',
  validate(
    yup.object().shape({
      params: yup.object().shape({
        userId: isMongoId(),
      }),
    })
  ),
  async (req: GetUserRequest, res, next) => {
    try {
      const user = await User.findById(req.params.userId)
        .populate('recipes')
        .populate('recipes.ingredients.ingredient');

      if (!user) return res.sendStatus(404);
      res.status(200).json(user);
    } catch (e) {
      next(e);
    }
  }
);

export default userController;
