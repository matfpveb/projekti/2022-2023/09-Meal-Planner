import * as fs from 'fs';
import { Request, Router } from 'express';
import { HydratedDocument } from 'mongoose';
import * as yup from 'yup';
import multer from 'multer';
import * as mime from 'mime-types';
import { nanoid } from 'nanoid';

// DB
import { Recipe, RecipeSchema } from '../database/models/recipe';

// Middlewares
import authenticate from '../middlewares/authenticate';
import validate from '../middlewares/validate';

// Validators
import { isMongoId, isRecipeName } from '../validation';
import { MeasureUnit } from '../database/types';
import imageUrl from '../util/imageUrl';
import { User } from '../database/models/user';

const recipesController = Router();
const photoUpload = multer({
  storage: multer.diskStorage({
    destination: 'public/images',
    filename(
      req: Request,
      file: Express.Multer.File,
      callback: (error: Error | null, filename: string) => void
    ) {
      const ext = mime.extension(file.mimetype);
      if (!ext) return callback(new Error('File invalid.'), file.filename);
      callback(null, `${nanoid()}.${ext}`);
    },
  }),
  fileFilter(
    req: Request,
    file: Express.Multer.File,
    callback: multer.FileFilterCallback
  ) {
    if (!['image/jpeg', 'image/png'].includes(file.mimetype)) {
      callback(null, false);
      return;
    }

    callback(null, true);
  },
});

type GetAllRequest = Request<
  { id?: string },
  HydratedDocument<RecipeSchema>[] | HydratedDocument<RecipeSchema>,
  {}
>;

// @TODO: use transactions here

recipesController.get(
  '/:id?',
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId().optional(),
      }),
    })
  ),
  async (req: GetAllRequest, res, next) => {
    let data;
    try {
     const { id } = req.params;
     if(id){
     const author = await User.findById(id).exec();
         data = await Recipe.find({ 'author': author })
      .populate('ingredients.ingredient')
      .populate('author');
    
     }else{
         data = await Recipe.find();
     }

     if (!data) return res.sendStatus(404);
     return res.status(200).json(data);

    } catch (e) {
      next(e);
    }
  }
);

type CreateRequest = Request<
  {},
  HydratedDocument<RecipeSchema>,
  Omit<RecipeSchema, 'author'>
>;

recipesController.post(
  '/',
  authenticate(),
  validate(
    yup.object().shape({
      body: yup.object().shape({
        name: isRecipeName(),
        img: yup.string().url().nullable().default(null),
        ingredients: yup
          .array()
          .of(
            yup.object().shape({
              ingredient: isMongoId(),
              amount: yup.number().positive(),
              unit: yup.string().oneOf(Object.values(MeasureUnit)),
            })
          )
          .required(),
        steps: yup.array().of(yup.string().min(1)),
      }),
    })
  ),
  async (req: CreateRequest, res, next) => {
    try {
      const recipe = new Recipe({ ...req.body, author: req.user });
      await recipe.save();
      req.user.recipes.push(recipe.id);
      await req.user.save();
      res.status(201).json(recipe);
    } catch (e) {
      next(e);
    }
  }
);

type UpdateRequest = Request<
  { id: string },
  HydratedDocument<RecipeSchema>,
  Partial<Omit<RecipeSchema, 'author'>>
>;

recipesController.patch(
  '/:id',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
      body: yup.object().shape({
        name: isRecipeName().optional(),
        img: yup.string().url().optional(),
        ingredients: yup
          .array()
          .of(
            yup.object().shape({
              ingredient: isMongoId(),
              amount: yup.number().positive(),
              unit: yup.string().oneOf(Object.values(MeasureUnit)),
            })
          )
          .optional(),
        steps: yup.array().of(yup.string().min(1)).optional(),
      }),
    })
  ),
  async (req: UpdateRequest, res, next) => {
    try {
      const recipe = await Recipe.findById(req.params.id);
      if (!recipe) return res.sendStatus(404);
      if (recipe.author.toString() !== req.user._id.toString())
        return res.sendStatus(401);

      recipe.set(req.body);
      await recipe.save();

      res.status(200).json(recipe);
    } catch (e) {
      next(e);
    }
  }
);

type DeleteRequest = Request<{ id: string }, HydratedDocument<RecipeSchema>>;

recipesController.delete(
  '/:id',
  authenticate(),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
    })
  ),
  async (req: DeleteRequest, res, next) => {
    try {
      const recipe = await Recipe.findById(req.params.id);
      if (!recipe) return res.sendStatus(404);
      if (recipe.author.toString() !== req.user._id.toString())
        return res.sendStatus(401);

      await Recipe.deleteOne({ _id: recipe._id });
      req.user.recipes = req.user.recipes.filter(
        (id) => id.toString() !== recipe.id.toString()
      );
      await req.user.save();

      res.status(200).json(recipe);
    } catch (e) {
      next(e);
    }
  }
);

type UploadPhotoRequest = Request<{ id: string }>;
recipesController.put(
  '/:id/photo',
  photoUpload.single('file'),
  async (req: UploadPhotoRequest, res, next) => {
    try {
      if (!req.file) return res.sendStatus(400);

      const recipe = await Recipe.findById(req.params.id);
      if (!recipe) {
        fs.unlinkSync(req.file.path);
        return res.sendStatus(404);
      }

      recipe.img = imageUrl(req, req.file.filename);
      await recipe.save();

      res.sendStatus(200);
    } catch (e) {
      next(e);
    }
  }
);

export default recipesController;
