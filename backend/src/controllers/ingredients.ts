import { Request, Response, Router } from 'express';
import { HydratedDocument } from 'mongoose';
import * as yup from 'yup';

// DB
import { Ingredient, IngredientSchema } from '../database/models/ingredient';
import { IngredientCategory, UserRole } from '../database/types';

// Middlewares
import authenticate from '../middlewares/authenticate';
import validate from '../middlewares/validate';

// Validators
import { isNutritiveValue, isMongoId, isIngredientName } from '../validation';

const ingredientsController = Router();

type GetAllRequest = Request<{}, HydratedDocument<IngredientSchema>[], {}>;

ingredientsController.get('/', async (req: GetAllRequest, res, next) => {
  const ingredients = await Ingredient.find();
  res.status(200).json(ingredients);
});

type CreateRequest = Request<
  {},
  HydratedDocument<IngredientSchema>,
  IngredientSchema
>;

ingredientsController.post(
  '/',
  authenticate(UserRole.Admin),
  validate(
    yup.object().shape({
      body: yup.object().shape({
        name: isIngredientName(),
        category: yup.string().oneOf(Object.values(IngredientCategory)),
        nutritiveValues: yup.object().shape({
          protein: isNutritiveValue(),
          carbs: isNutritiveValue(),
          fat: isNutritiveValue(),
        }),
      }),
    }),
    {
      abortEarly: false,
    }
  ),
  async (req: CreateRequest, res, next) => {
    try {
      const ingredient = await Ingredient.create(req.body);
      res.status(201).json(ingredient);
    } catch (e) {
      next(e);
    }
  }
);

type UpdateRequest = Request<
  { id: string },
  HydratedDocument<IngredientSchema>,
  Partial<IngredientSchema>
>;

ingredientsController.patch(
  '/:id',
  authenticate(UserRole.Admin),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
      body: yup.object().shape({
        name: isIngredientName().optional(),
        category: yup
          .string()
          .oneOf(Object.values(IngredientCategory))
          .optional(),
        nutritiveValues: yup
          .object()
          .shape({
            protein: isNutritiveValue().optional(),
            carbs: isNutritiveValue().optional(),
            fat: isNutritiveValue().optional(),
          })
          .optional(),
      }),
    })
  ),
  async (req: UpdateRequest, res: Response, next) => {
    try {
      const ingredient = await Ingredient.findById(req.params.id);
      if (!ingredient) {
        return res.sendStatus(404);
      }
      const nutritiveValues = {
        ...ingredient.nutritiveValues,
        ...req.body.nutritiveValues,
      };
      ingredient.set({ ...req.body, nutritiveValues });
      await ingredient.save();

      res.status(200).json(ingredient);
    } catch (e) {
      next(e);
    }
  }
);

type DeleteRequest = Request<
  { id: string },
  HydratedDocument<IngredientSchema>
>;

ingredientsController.delete(
  '/:id',
  authenticate(UserRole.Admin),
  validate(
    yup.object().shape({
      params: yup.object().shape({
        id: isMongoId(),
      }),
    })
  ),
  async (req: DeleteRequest, res: Response, next) => {
    try {
      const deletedIngredient = await Ingredient.findByIdAndDelete(
        req.params.id
      );

      if (!deletedIngredient) {
        return res.sendStatus(404);
      }

      res.status(200).json(deletedIngredient);
    } catch (e) {
      next(e);
    }
  }
);

export default ingredientsController;
