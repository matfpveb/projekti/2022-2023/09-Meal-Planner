import { NextFunction, Request, Response, Router } from 'express';
import bcrypt from 'bcrypt';
import * as yup from 'yup';

// DB
import { User } from '../database/models/user';

// Middlewares
import validate from '../middlewares/validate';

// Utils
import { generateJWT } from '../util/jwt';

// Validation
import { isPassword, isUsername, isEmail } from '../validation';

const authController = Router();

type LoginRequest = Request<
  {},
  { token: string } | { message: string },
  { username: string; password: string }
>;

authController.post(
  '/login',
  validate(
    yup.object().shape({
      body: yup.object().shape({
        username: isUsername(false),
        password: isPassword(),
      }),
    })
  ),
  async (req: LoginRequest, res: Response, next) => {
    const { username, password } = req.body;

    try {
      const user = await User.findOne({ username }).select('+password');

      // @ts-ignore
      if (!user || !(await bcrypt.compare(password, user.password))) {
        return res
          .status(401)
          .json({ message: 'Incorrect username or password' });
      }

      const token = generateJWT({
        username,
      });

      return res.send({ token });
    } catch (e) {
      next(e);
    }
  }
);

type SignUpRequest = Request<
  {},
  { token: string },
  { username: string; password: string; email: string }
>;

authController.post(
  '/signup',
  validate(
    yup.object().shape({
      body: yup.object().shape({
        username: isUsername(),
        email: isEmail(),
        password: isPassword(),
      }),
    })
  ),
  async (req: SignUpRequest, res: Response, next: NextFunction) => {
    const { username, email, password } = req.body;

    try {
      const hashedPassword = await bcrypt.hash(password, 12);

      const user = new User({
        username,
        email,
        password: hashedPassword,
      });

      await user.save();

      const token = generateJWT({
        username,
      });

      res.send({ token });
    } catch (e) {
      next(e);
    }
  }
);

export default authController;
