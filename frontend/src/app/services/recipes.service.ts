import { Injectable } from '@angular/core';
import { MeasureUnit } from '../models/ingredient';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Recipe } from '../models/recipe';
import { Observable, switchMap } from 'rxjs';

interface AddRecipeParams {
  name: string;
  ingredients: {
    ingredient: string;
    amount: number;
    unit: MeasureUnit;
  }[];
  steps: string[];
}

@Injectable({
  providedIn: 'root',
})
export class RecipesService {
  private readonly Urls = {
    AddRecipe: `${environment.apiUrl}/recipes`,
    UploadPhoto: (recipeId: string) =>
      `${environment.apiUrl}/recipes/${recipeId}/photo`,
    GetRecipes: `${environment.apiUrl}/recipes`
  };

  constructor(private http: HttpClient) {}

  addRecipe(params: AddRecipeParams) {
    return this.http.post<Recipe>(this.Urls.AddRecipe, params);
  }

  uploadPhoto(photo: File, recipeId: string) {
    const formData = new FormData();
    formData.append('file', photo, photo.name);

    return this.http.put<undefined>(this.Urls.UploadPhoto(recipeId), formData);
  }
  private baseUrl = 'http://localhost:8000'; 

   getCurrentUser(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/user/current`);
  }

  getRecipesByCurrentUser(): Observable<any> {
   return this.getCurrentUser().pipe(
  switchMap(user => this.http.get<any>(`${this.baseUrl}/recipes/${user._id}`))
);

  }
  getRecipes() {
    return this.http.get<Recipe[]>(this.Urls.GetRecipes);
  }

}
