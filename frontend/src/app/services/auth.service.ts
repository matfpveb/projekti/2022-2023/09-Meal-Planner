import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { User } from '../models/user';
import { tap, Subject, mergeMap } from 'rxjs';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly Urls = {
    Login: `${environment.apiUrl}/auth/login`,
    SignUp: `${environment.apiUrl}/auth/signup`,
    CurrentUser: `${environment.apiUrl}/user/current`,
  };

  private readonly userSubject = new Subject<User | null>();
  private loggedIn = false;
  public readonly user = this.userSubject.asObservable();

  constructor(private http: HttpClient, private jwt: JwtService) {
    this.loggedIn = this.jwt.getToken() !== null;
  }

  login(username: string, password: string) {
    return this.http
      .post<{ token: string }>(this.Urls.Login, {
        username,
        password,
      })
      .pipe(
        // catchError(this.handleHttpError),
        tap(({ token }) => this.jwt.setToken(token)),
        mergeMap(() => this.getCurrentUser()),
        tap((user) => {
          this.userSubject.next(user);
          this.loggedIn = true;
        })
      );
  }

  signup(username: string, email: string, password: string) {
    return this.http
      .post<{ token: string }>(this.Urls.SignUp, {
        username,
        email,
        password,
      })
      .pipe(
        // catchError(this.handleHttpError),
        tap(({ token }) => this.jwt.setToken(token)),
        mergeMap(() => this.getCurrentUser()),
        tap((user) => {
          this.userSubject.next(user);
          this.loggedIn = true;
        })
      );
  }

  logout() {
    this.jwt.removeToken();
    this.loggedIn = false;
    this.userSubject.next(null);
  }

  isUserLoggedIn() {
    return this.loggedIn;
  }

  private handleHttpError(error: HttpErrorResponse) {
    console.log(error);
    // @TODO: not implemented
    throw error;
  }

  private getCurrentUser() {
    return this.http.get<User>(this.Urls.CurrentUser, undefined);
  }
}
