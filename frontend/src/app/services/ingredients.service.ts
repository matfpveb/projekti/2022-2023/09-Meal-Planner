import { Injectable } from '@angular/core';
import { Ingredient } from '../models/ingredient';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class IngredientsService {
  private readonly Urls = {
    GetAll: `${environment.apiUrl}/ingredients`,
  };

  constructor(private http: HttpClient) {}

  getAll() {
    // @TODO: error handling
    return this.http.get<Ingredient[]>(this.Urls.GetAll, undefined);
  }
}
