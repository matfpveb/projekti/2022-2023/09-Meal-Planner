import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  private readonly TokenKey = 'jwt-token';

  constructor() {}

  setToken(token: string) {
    localStorage.setItem(this.TokenKey, token);
  }

  getToken() {
    return localStorage.getItem(this.TokenKey);
  }

  removeToken() {
    localStorage.removeItem(this.TokenKey);
  }
}
