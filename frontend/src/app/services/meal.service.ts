import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MealService {
  private readonly apiUrl = environment.apiUrl;
  private readonly mealUrl = `${this.apiUrl}/meals`;

  constructor(private http: HttpClient) { }

  getRecipes(): Observable<any> {
    const url = `${this.apiUrl}/recipes`;
    return this.http.get<any>(url);
  }

  getMeals(): Observable<any> {
    return this.http.get<any>(this.mealUrl);
  }

  createMeal(meal: any): Observable<any> {
    console.log(meal);
    return this.http.post<any>(this.mealUrl, meal);
  }
}
