import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { RecipesService } from 'src/app/services/recipes.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent {
  constructor(private auth: AuthService, private router: Router, private recipeService:RecipesService) {}
  recipes :any=[];
  
  ngOnInit() {
    this.getRecipesByCurrentUser();
  }

  getRecipesByCurrentUser() {
    this.recipeService.getRecipesByCurrentUser().subscribe(
      (data) => {
        this.recipes = data;
      },
      (error) => {
        console.log('Došlo je do greške prilikom dohvatanja recepata:', error);
      }
    );
  }
  logout() {
    this.auth.logout();
    this.router.navigateByUrl('login');
  }
}



