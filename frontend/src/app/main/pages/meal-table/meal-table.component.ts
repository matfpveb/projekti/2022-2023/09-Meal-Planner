import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-meal-table',
  templateUrl: './meal-table.component.html',
  styleUrls: ['./meal-table.component.css']
})
export class MealTableComponent {
  meals: any[] = [];
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getMeals();
  }
 
  getMeals(): void {
    this.http.get<any[]>('http://localhost:8000/meals').subscribe(
      (response) => {
        this.meals = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
