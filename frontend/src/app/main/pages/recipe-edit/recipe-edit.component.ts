import { Component, OnDestroy } from '@angular/core';
import {
  concat,
  concatWith,
  empty,
  mergeMap,
  Observable,
  of,
  Subscription,
} from 'rxjs';
import { Ingredient, MeasureUnit } from '../../../models/ingredient';
import { IngredientsService } from '../../../services/ingredients.service';
import { FormBuilder } from '@angular/forms';
import { RecipesService } from '../../../services/recipes.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css'],
})
export class RecipeEditComponent implements OnDestroy {
  ingredients: Observable<Ingredient[]> = this.ingredientsService.getAll();
  measureUnits = Object.values(MeasureUnit);

  private recipeSub: Subscription | null = null;

  // @TODO: add validation
  addRecipeForm = this.fb.group({
    name: [''],
    steps: this.fb.array([this.fb.control('')]),
    ingredients: this.fb.array([
      this.fb.group({
        id: [''],
        amount: [''],
        unit: [''],
      }),
    ]),
  });
  recipeImage: File | null = null;

  constructor(
    private ingredientsService: IngredientsService,
    private recipesService: RecipesService,
    private snackBar: MatSnackBar,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnDestroy() {
    this.recipeSub?.unsubscribe();
  }

  get stepsFormArray() {
    return this.addRecipeForm.controls.steps;
  }

  get ingredientsFormArray() {
    return this.addRecipeForm.controls.ingredients;
  }

  addStep() {
    this.stepsFormArray.push(this.fb.control(''));
  }

  addIngredient() {
    this.ingredientsFormArray.push(
      this.fb.group({
        id: [''],
        amount: [''],
        unit: [''],
      })
    );
  }

  addRecipe() {
    const { name, steps, ingredients } = this.addRecipeForm.value;
    this.recipeSub = this.recipesService
      .addRecipe({
        name: name!,
        steps: steps as string[],
        ingredients: ingredients!.map((v) => ({
          ingredient: v.id!,
          amount: parseFloat(v.amount!),
          unit: v.unit as MeasureUnit,
        })),
      })
      .pipe(
        mergeMap((recipe) => {
          if (!this.recipeImage) {
            this.snackBar.open('Slika recepta je obavezna.', 'U redu', {
              duration: 3000,
            });
            return of();
          }

          return concat(
            of(recipe),
            this.recipesService.uploadPhoto(this.recipeImage, recipe._id)
          );
        })
      )
      .subscribe((recipe) => {
        if (recipe) {
          console.log('successfully added recipe', recipe);
          this.snackBar.open('Uspešno ste napraviili novi recept.', 'U redu', {
            duration: 3000,
          });
          this.router.navigateByUrl('/profile');
        }
      });
  }

  uploadPhoto() {}

  onPhotoChange(event: Event) {
    this.recipeImage = (event.target as HTMLInputElement).files![0];
  }
}
