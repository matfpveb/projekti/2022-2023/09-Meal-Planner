import { Component } from '@angular/core';
import { FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/auth.service';

const samePassword: ValidatorFn = (control) => {
  const password = control.get('password');
  const repeatPassword = control.get('repeatPassword');

  if (password && repeatPassword && password.value === repeatPassword.value)
    return null;

  return { repeatPassword: { value: password } };
};

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  // @TODO: improve validations
  signupForm = this.fb.group(
    {
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      repeatPassword: ['', [Validators.required]],
    },
    { validators: samePassword }
  );

  signupSub: Subscription | null = null;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  signup() {
    const { username, email, password } = this.signupForm.value;
    this.signupSub = this.auth
      .signup(username!, email!, password!)
      .subscribe((user) => {
        console.log('Korisnik je uspešno registrovan: ', user);
        this.snackBar.open('Uspešno ste se registrovali.', 'U redu', {
          duration: 3000,
        });
        this.router.navigateByUrl('/');
      });
  }
}
