import { Subscription } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnDestroy {
  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  loginSub: Subscription | null = null;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnDestroy() {
    this.loginSub?.unsubscribe();
  }

  login() {
    const { username, password } = this.loginForm.value;
    this.loginSub = this.auth.login(username!, password!).subscribe((user) => {
      console.log('Korisnik je uspešno prijavljen: ', user);
      this.snackBar.open('Uspešno ste se prijavili.', 'U redu', {
        duration: 3000,
      });
      this.router.navigateByUrl('/');
    });
  }
}
