import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent {
  ingridients: any[] = [];
  date:string="";
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
  }
 
  getIngridients(): void {
    console.log(this.date);
    this.http.get<any[]>(`http://localhost:8000/meals/ingridients/${this.date}`).subscribe(
      (response) => {
        this.ingridients = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
