import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';

@Component({
  selector: 'app-meal-page',
  templateUrl: './meal-page.component.html',
  styleUrls: ['./meal-page.component.css']
})
export class MealPageComponent implements OnInit {
  meal = {
    date: '',
    mealType: '',
    recipeId: ''
  };
  recipes: any=[]; 
  meals: any=[]; 

  constructor(private mealService: MealService) { }

  ngOnInit() {
    this.fetchRecipes();
    this.fetchMeals();
  }

  addMeal() {
    this.mealService.createMeal(this.meal).subscribe(() => {
      this.fetchMeals();
      this.resetForm();
    });
  }

  fetchRecipes() {
    this.mealService.getRecipes().subscribe((recipes) => {
      this.recipes = recipes;
    });
  }

  fetchMeals() {
    this.mealService.getMeals().subscribe((meals) => {
      this.meals = meals;
    });
  }

  resetForm() {
    this.meal = {
      date: '',
      mealType: '',
      recipeId: ''
    };
  }
}
