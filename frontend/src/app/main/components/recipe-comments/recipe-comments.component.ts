import { Component } from '@angular/core';

@Component({
  selector: 'recipe-comments',
  templateUrl: './recipe-comments.component.html',
  styleUrls: ['./recipe-comments.component.css'],
})
export class RecipeCommentsComponent {}
