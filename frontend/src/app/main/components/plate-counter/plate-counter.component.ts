import { Component } from '@angular/core';

@Component({
  selector: 'plate-counter',
  templateUrl: './plate-counter.component.html',
  styleUrls: ['./plate-counter.component.css'],
})
export class PlateCounterComponent {}
