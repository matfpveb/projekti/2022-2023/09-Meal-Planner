import { Component } from '@angular/core';

@Component({
  selector: 'add-comment-form',
  templateUrl: './add-comment-form.component.html',
  styleUrls: ['./add-comment-form.component.css'],
})
export class AddCommentFormComponent {}
