import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { MainRoutingModule } from './main-routing.module';
import { AdminModule } from '../admin/admin.module';
import { SignupComponent } from './pages/signup/signup.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RecipeCardComponent } from './components/recipe-card/recipe-card.component';
import { RecipeDetailsComponent } from './pages/recipe-details/recipe-details.component';
import { PlateCounterComponent } from './components/plate-counter/plate-counter.component';
import { MenuComponent } from '../components/menu/menu.component';
import { ShoppingListComponent } from './pages/shopping-list/shopping-list.component';
import { RecipeEditComponent } from './pages/recipe-edit/recipe-edit.component';
import { MealPageComponent } from './pages/meal-page/meal-page.component';
import { MealTableComponent } from './pages/meal-table/meal-table.component';


import { AngularMaterialModule } from '../angular-material.module';
import { RecipeCommentsComponent } from './components/recipe-comments/recipe-comments.component';
import { AddCommentFormComponent } from './components/forms/add-comment-form/add-comment-form.component';
import { LoadingComponent } from './components/loading/loading.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SignupComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    MenuComponent,
    RecipeCardComponent,
    RecipeDetailsComponent,
    PlateCounterComponent,
    ShoppingListComponent,
    RecipeEditComponent,
    RecipeCommentsComponent,
    AddCommentFormComponent,
    LoadingComponent,
    MealPageComponent, 
    MealTableComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MainRoutingModule,
    AngularMaterialModule,
    NgxMatSelectSearchModule,
    FormsModule,
  ],
  exports: [MenuComponent, AngularMaterialModule],
})
export class MainModule {}
