import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Guards
import { GuestGuard } from '../guards/guest.guard';
import { AuthenticatedGuard } from '../guards/authenticated.guard';

// Pages
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RecipeEditComponent } from './pages/recipe-edit/recipe-edit.component';
import { RecipeDetailsComponent } from './pages/recipe-details/recipe-details.component';
import { ShoppingListComponent } from './pages/shopping-list/shopping-list.component';
import { RecipeListComponent } from './pages/recipe-list/recipe-list.component';
import { MealPageComponent } from './pages/meal-page/meal-page.component';
import { MealTableComponent } from './pages/meal-table/meal-table.component';
// Admin Pages
import { AdminPanelComponent } from '../admin/pages/admin-panel/admin-panel.component';
import { IngredientsComponent } from '../admin/pages/ingredients/ingredients.component';
import { UsersComponent } from '../admin/pages/users/users.component';
import { RecipesComponent } from '../admin/pages/recipes/recipes.component';
import { AdminHomeComponent } from '../admin/pages/admin-home/admin-home.component';
import { AddIngredientComponent } from '../admin/pages/add-ingredient/add-ingredient.component';

const routes: Routes = [
  {
    path: 'recipe-edit',
    component: RecipeEditComponent,
    canActivate: [AuthenticatedGuard],
  },
  { path: 'recipe-details', component: RecipeDetailsComponent },
  { path: 'recipe-list', component:RecipeListComponent},
  { path: 'meal-page', component:MealPageComponent},
  { path: 'meal-table', component: MealTableComponent},
  {
    path: 'shopping-list',
    component: ShoppingListComponent,
    canActivate: [AuthenticatedGuard],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthenticatedGuard],
  },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [GuestGuard] },
  { path: 'signup', component: SignupComponent, canActivate: [GuestGuard] },
  // @TODO: add AdminGuard below
  {
    path: 'admin',
    component: AdminPanelComponent,
    children: [
      {
        path: '',
        component: AdminHomeComponent,
      },
      {
        path: 'ingredients',
        component: IngredientsComponent,
      },
      {
        path: 'add-ingredient',
        component: AddIngredientComponent,
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'recipes',
        component: RecipesComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
