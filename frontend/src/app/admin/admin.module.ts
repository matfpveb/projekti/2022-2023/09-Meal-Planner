import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { AngularMaterialModule } from '../angular-material.module';
import { RouterLink, RouterOutlet } from '@angular/router';
import { IngredientsComponent } from './pages/ingredients/ingredients.component';
import { UsersComponent } from './pages/users/users.component';
import { RecipesComponent } from './pages/recipes/recipes.component';
import { AdminHomeComponent } from './pages/admin-home/admin-home.component';
import { AddIngredientComponent } from './pages/add-ingredient/add-ingredient.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AdminPanelComponent,
    IngredientsComponent,
    UsersComponent,
    RecipesComponent,
    AdminHomeComponent,
    AddIngredientComponent,
    
  ],
  imports: [CommonModule, AngularMaterialModule, RouterOutlet, RouterLink,FormsModule],
  exports: [AdminPanelComponent],
})
export class AdminModule {}
