import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
})


export class RecipesComponent implements OnInit {
 
  recipes: any[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getRecipes();
  }
 
  getRecipes(): void {
    this.http.get<any[]>('http://localhost:8000/recipes').subscribe(
      (response) => {
        this.recipes = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
