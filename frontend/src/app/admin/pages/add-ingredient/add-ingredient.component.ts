import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.css'],
})
export class AddIngredientComponent {
  ingredient: any = {};

  constructor(private http: HttpClient) {}

  addIngredient() {
    this.http.post<any>('http://localhost:8000/ingredients', this.ingredient).subscribe(
      response => {
        // Sastojak uspešno dodat u bazu
        console.log('Sastojak dodat:', response);
        // Resetujemo formu
        this.ingredient = {};
      },
      error => {
        // Greška prilikom dodavanja sastojka
        console.error('Greška pri dodavanju sastojka:', error);
      }
    );
  }
}



