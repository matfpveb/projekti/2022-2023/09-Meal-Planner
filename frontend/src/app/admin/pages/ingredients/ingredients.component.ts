import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css'],
})
export class IngredientsComponent implements OnInit { 
  ingredients: any[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getIngredients();
  }
 
  getIngredients(): void {
    this.http.get<any[]>('http://localhost:8000/ingredients').subscribe(
      (response) => {
        this.ingredients = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }}




