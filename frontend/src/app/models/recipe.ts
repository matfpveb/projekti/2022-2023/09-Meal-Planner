import { User } from './user';
import { Ingredient } from './ingredient';

export interface Recipe {
  _id: string;
  name: string;
  author: User;
  img: string;
  ingredients: Ingredient[];
  steps: string[];
  comments: {
    user: string;
    content: string;
    id: string;
  }[];
  likes: []; // @TODO
}
