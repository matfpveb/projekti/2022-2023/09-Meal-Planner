export interface Ingredient {
  _id: string;
  name: string;
  category: IngredientCategory;
  nutritiveValues: {
    protein: number;
    carbs: number;
    fat: number;
  };
}

export enum MeasureUnit {
  G = 'G',
  Kg = 'Kg',
  L = 'L',
  'Ml' = 'Ml',
  Tsp = 'Tsp',
  Tbsp = 'Tbsp',
}

export enum IngredientCategory {
  Butter = 'Butter',
  MilkProduct = 'MilkProduct',
  EggProduct = 'EggProduct',
  Topping = 'Topping',
  Other = 'Other',
  Spice = 'Spice',
  MeetProduct = 'MeetProduct',
}
