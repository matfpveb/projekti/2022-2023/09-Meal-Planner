import { Recipe } from './recipe';

export interface Meal {
  _id: string;
  date: Date; 
  mealType: string;
  recipe: Recipe[];
}
