export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}

export interface User {
  _id: string;
  username: string;
  email: string;
  role: UserRole;

  recipes: []; // @TODO
  likedRecipes: []; // @TODO
}
