# Project Meal Planner

# Build procedura

Potrebno za pokretanje projekta:
- Node 18, npm
- MongoDB

## Koraci:

### Pokretanje backend-a:
 1. `cd backend`
 2. `cp .env.example .env` i konfigurisati sve `env` promenljive. Primer konfiguracije `env` promenljivih:
    ```
    PORT=8000

    # DB
    MONGODB_CONNECTION=mongodb://127.0.0.1:27017/mealplanner?directConnection=true&serverSelectionTimeoutMS=2000

    # JWT
    JWT_SECRET=c04362f26389db28f1c42e34ad6906efd8e84fb87b810402851f8e3841719459
    JWT_EXPIRES_IN=1h

    ```
    Sve promenljive mogu ostati kako su podešene i u primeru, jedino se mora postaviti dobar `MONGODB_CONNECTION`.
3. Popuniti bazu sa osnovnim podacima: `npm run db:seed`. Ovo će napraviti jednog `admin` korisnika i popuniti sve sastojke u bazi. Kredencijali:
   ```
   Username: admin
   Password: Admin123.
   ```
4. `npm start` - pokretanje servera

### Pokretanje frontend-a:
1. `cd frontend`
2. `npm install`
3. Postaviti promenljive u `src/environments/environment.ts`
3. `npm start`

# Baza podataka

```plantuml
@startuml

!theme plain
top to bottom direction
skinparam linetype ortho

class ingredients {
   _id: objectid
   category: string
   name: string
   nutritiveValues: object
   __v: int32
   nutritiveValues.carbs: double
   nutritiveValues.fat: double
   nutritiveValues.protein: double
}
class recipes {
   _id: objectid
   __v: int32
   author: objectid
   comments: list
   img: string
   likes: list
   name: string
   steps: array
   comments._id: objectid
   ingredients: list
   comments.content: string
   ingredients._id: objectid
   comments.user: objectid
   ingredients.amount: int32
   ingredients.ingredient: objectid
   ingredients.unit: string
}
class users {
   _id: objectid
   __v: int32
   email: string
   password: string
   username: string
   likedRecipes: list
   recipes: list
   role: string
}

@enduml

```
